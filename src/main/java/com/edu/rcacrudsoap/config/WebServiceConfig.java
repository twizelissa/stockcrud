package com.edu.rcacrudsoap.config;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

public class WebServiceConfig {
    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(messageDispatcherServlet, "/ws/elissa/stocks/*");
    }

    @Bean(name = "item-details")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema studentsSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("ItemsPort");
        definition.setTargetNamespace("http://elissa.com/stock/item");
        definition.setLocationUri("/ws/elissa/stocks");
        definition.setSchema(studentsSchema);
        return definition;
    }

    @Bean(name = "supplier")
    public Wsdl11Definition wsdl11Definition (XsdSchema studentsSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SuppliersPort");
        definition.setTargetNamespace("http://elissa.com/stock/supplier");
        definition.setLocationUri("/ws/elissa/stocks");
        definition.setSchema(studentsSchema);
        return definition;
    }
//
    @Bean
    public XsdSchema ItemSchema() {
        return new SimpleXsdSchema(new ClassPathResource("item-details.xsd"));
    }

    @Bean
    public XsdSchema SupplierSchema() {
        return new SimpleXsdSchema(new ClassPathResource("supplier-details.xsd"));
    }
}
