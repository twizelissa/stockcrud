package com.edu.rcacrudsoap.repositories;


import com.edu.rcacrudsoap.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
}
