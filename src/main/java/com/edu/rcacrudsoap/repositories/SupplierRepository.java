package com.edu.rcacrudsoap.repositories;

import com.edu.rcacrudsoap.models.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
