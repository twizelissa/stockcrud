package com.edu.rcacrudsoap.models;

public enum Status {
    NEW, GOOD_SHAPE, OLD
}
