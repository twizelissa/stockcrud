package com.edu.rcacrudsoap.models;

import javax.persistence.*;

@Entity
@Table(name="items")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

   public String name;
   public String itemCode;
   public String status;
   public int price;
   public String supplier;


    public Item() {
    }
   public Item(int id, String name, String itemCode, String status, int price, String supplier){
       this.id = id;
       this.name = name;
       this.itemCode = itemCode;
       this.status = status;
       this.price = price;
       this.supplier = supplier;
   }

    public Item( String name, String itemCode, String status, int price, String supplier){
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

}
