package com.edu.rcacrudsoap.endpoint;

import com.edu.rcacrudsoap.repositories.ItemRepository;
import com.elissa.stock.item.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndpoint {
    private final ItemRepository itemRepository;

    @Autowired
    public ItemEndpoint(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }


    @PayloadRoot(namespace = "http://elissa.com/stock/item", localPart = "NewItemRequest")
    @ResponsePayload
    public NewItemResponse create(@RequestPayload NewItemRequest request) {
        Item newItem = request.getItem();

        com.edu.rcacrudsoap.models.Item itemToSave = new com.edu.rcacrudsoap.models.Item(newItem.getName(), newItem.getItemCode(), newItem.getStatus(),newItem.getPrice(), newItem.getSupplier());
        itemRepository.save(itemToSave);
        NewItemResponse response = new NewItemResponse();
        response.setItem(newItem);

        return response;
    }

    @PayloadRoot(namespace = "http://elissa.com/stock/item", localPart = "GetAllItemsRequest")
    @ResponsePayload
    public GetAllItemsResponse findAll() {

        List<com.edu.rcacrudsoap.models.Item> items = itemRepository.findAll();

        GetAllItemsResponse response = new GetAllItemsResponse();

        for (com.edu.rcacrudsoap.models.Item item : items) {
            Item foundItem = mapItem(item);

            response.getItem().add(foundItem);
        }

        return response;
    }

    @PayloadRoot(namespace = "http://elissa.com/stock/item", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request) {
        Optional<com.edu.rcacrudsoap.models.Item> _item = itemRepository.findById(request.getId());

        if (!_item.isPresent())
            return new GetItemDetailsResponse();

        com.edu.rcacrudsoap.models.Item item = _item.get();

        GetItemDetailsResponse response = new GetItemDetailsResponse();

        Item __item = mapItem(item);

        response.setItem(__item);

        return response;
    }

    @PayloadRoot(namespace = "http://elissa.com/stock/item", localPart = "DeleteItemRequest")
    @ResponsePayload
    public DeleteItemResponse delete(@RequestPayload DeleteItemRequest request) {
        itemRepository.deleteById(request.getId());
        DeleteItemResponse response = new DeleteItemResponse();
        response.setMessage("Successfully deleted");
        return response;
    }


    @PayloadRoot(namespace = "http://elissa.com/stock/item", localPart = "UpdateItemRequest")
    @ResponsePayload
    public UpdateItemResponse update(@RequestPayload UpdateItemRequest request) {
        Item __item = request.getItem();

        com.edu.rcacrudsoap.models.Item _item = mapItemUpdate(__item);
        __item.setId(__item.getId());

        itemRepository.save(_item);

        UpdateItemResponse itemDto = new UpdateItemResponse();

        itemDto.setItem(__item);

        return itemDto;
    }

    private com.edu.rcacrudsoap.models.Item mapItemUpdate(Item __item) {
        com.edu.rcacrudsoap.models.Item item = new com.edu.rcacrudsoap.models.Item();
        item.setId(__item.getId());
        item.setName(__item.getName());
        item.setItemCode(__item.getItemCode());
        item.setStatus(__item.getStatus());
        item.setPrice(__item.getPrice());
        item.setSupplier(__item.getSupplier());

        return item;
    }

    private Item mapItem(com.edu.rcacrudsoap.models.Item __item) {
        Item item = new Item();
        item.setId(__item.getId());
        item.setName(__item.getName());
        item.setItemCode(__item.getItemCode());
        item.setStatus(__item.getStatus());
        item.setPrice(__item.getPrice());
        item.setSupplier(__item.getSupplier());

        return item;
    }
}

