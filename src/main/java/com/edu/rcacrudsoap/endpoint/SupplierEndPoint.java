package com.edu.rcacrudsoap.endpoint;


import com.edu.rcacrudsoap.repositories.SupplierRepository;
import com.elissa.stock.supplier.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SupplierEndPoint {
    private final SupplierRepository supplierRepository;

    @Autowired
    public SupplierEndPoint(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @PayloadRoot(namespace = "http://elissa.com/stock/supplier", localPart = "NewSupplierRequest")
    @ResponsePayload
    public NewSupplierResponse create(@RequestPayload NewSupplierRequest request) {
        Supplier newSupplier = request.getSupplier();

        com.edu.rcacrudsoap.models.Supplier supplierToSave = new com.edu.rcacrudsoap.models.Supplier(newSupplier.getFirstName(),newSupplier.getLastName(),newSupplier.getEmail(),newSupplier.getMobile());
        supplierRepository.save(supplierToSave);
        NewSupplierResponse response = new NewSupplierResponse();
        response.setSupplier(newSupplier);

        return response;
    }

    @PayloadRoot(namespace = "http://elissa.com/stock/supplier", localPart = "GetAllSuppliersRequest")
    @ResponsePayload
    public GetAllSuppliersResponse findAll() {

        List<com.edu.rcacrudsoap.models.Supplier> suppliers = supplierRepository.findAll();

        GetAllSuppliersResponse response = new GetAllSuppliersResponse();

        for (com.edu.rcacrudsoap.models.Supplier supplier : suppliers) {
            Supplier foundSuppliers = mapSupplier(supplier);

            response.getSupplier().add(foundSuppliers);
        }

        return response;
    }


    @PayloadRoot(namespace = "http://elissa.com/stock/supplier", localPart = "GetSupplierDetailsRequest")
    @ResponsePayload
    public GetSupplierDetailsResponse findById(@RequestPayload GetSupplierDetailsRequest request) {
        Optional<com.edu.rcacrudsoap.models.Supplier> _supplier = supplierRepository.findById(request.getId());

        if (!_supplier.isPresent())
            return new GetSupplierDetailsResponse();

        com.edu.rcacrudsoap.models.Supplier supplier = _supplier.get();

        GetSupplierDetailsResponse response = new GetSupplierDetailsResponse();

        Supplier __supplier = mapSupplier(supplier);

        response.setSupplier(__supplier);

        return response;
    }

    @PayloadRoot(namespace = "http://elissa.com/stock/supplier", localPart = "DeleteSupplierRequest")
    @ResponsePayload
    public DeleteSupplierResponse delete(@RequestPayload DeleteSupplierRequest request) {
        supplierRepository.deleteById(request.getId());
        DeleteSupplierResponse response = new DeleteSupplierResponse();
        response.setMessage("Successfully deleted");
        return response;
    }


    @PayloadRoot(namespace = "http://elissa.com/stock/supplier", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSupplierRequest request) {
        Supplier __supplier = request.getSupplier();

        com.edu.rcacrudsoap.models.Supplier _supplier = mapSupplierUpdate(__supplier);
        __supplier.setId(__supplier.getId());

        supplierRepository.save(_supplier);

        UpdateSupplierResponse supplierDto = new UpdateSupplierResponse();

        supplierDto.setSupplier(__supplier);

        return supplierDto;
    }


    private com.edu.rcacrudsoap.models.Supplier mapSupplierUpdate(Supplier __supplier) {
        com.edu.rcacrudsoap.models.Supplier supplier = new com.edu.rcacrudsoap.models.Supplier();
        supplier.setId(__supplier.getId());
        supplier.setFirstName(__supplier.getFirstName());
        supplier.setLastName(__supplier.getLastName());
        supplier.setEmail(__supplier.getEmail());
        supplier.setMobile(__supplier.getMobile());

        return supplier;
    }

    private Supplier mapSupplier(com.edu.rcacrudsoap.models.Supplier __supplier) {
        Supplier supplier = new Supplier();
        supplier.setId(__supplier.getId());
        supplier.setFirstName(__supplier.getFirstName());
        supplier.setLastName(__supplier.getLastName());
        supplier.setEmail(__supplier.getEmail());
        supplier.setMobile(__supplier.getMobile());

        return supplier;
    }
}
