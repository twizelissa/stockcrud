package com.edu.rcacrudsoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RcaCrudSoapApplication {

    public static void main(String[] args) {
        SpringApplication.run(RcaCrudSoapApplication.class, args);
    }

}
